package com.barcodedetectorview.sample

import android.Manifest.permission.CAMERA
import android.content.Intent
import android.content.Intent.ACTION_MAIN
import android.content.Intent.CATEGORY_HOME
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.M
import android.os.Bundle
import android.view.View.SYSTEM_UI_FLAG_LOW_PROFILE
import android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
import androidx.appcompat.app.AppCompatActivity
import com.barcodedetectorview.Barcode
import com.barcodedetectorview.BarcodeDetectorView
import kotlinx.android.synthetic.main.activity_sample.*

class SampleActivity : AppCompatActivity(), BarcodeDetectorView.Callback {
    private var isResumed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        window.addFlags(FLAG_TRANSLUCENT_STATUS)
        detectorView.addCallback(this)
    }

    override fun onResume() {
        super.onResume()
        isResumed = true
        detectorView.tryStart()
    }

    override fun onPause() {
        super.onPause()
        isResumed = false
        detectorView.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        detectorView.shutdown()
    }

    override fun onDetected(barcodes: List<Barcode>) {
        textView.text = barcodes.joinToString { it.format.name }
    }

    override fun onBackPressed() {
        startActivity(Intent(ACTION_MAIN).addCategory(CATEGORY_HOME))
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (permissions.getOrNull(0) == CAMERA && grantResults[0] == PERMISSION_GRANTED) {
            if (isResumed) detectorView.start()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_LOW_PROFILE
    }

    private fun BarcodeDetectorView.tryStart() {
        if (SDK_INT >= M && checkSelfPermission(CAMERA) != PERMISSION_GRANTED) {
            requestPermissions(arrayOf(CAMERA), 0)
        } else {
            start()
        }
    }
}
