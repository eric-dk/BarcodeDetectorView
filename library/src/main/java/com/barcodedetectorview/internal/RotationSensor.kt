@file:Suppress("deprecation")

package com.barcodedetectorview.internal

import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.hardware.Camera
import android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
import android.view.Display
import android.view.OrientationEventListener
import android.view.Surface.*
import android.view.WindowManager

internal class RotationSensor(context: Context,
                              private val config: Camera.CameraInfo,
                              private val onRotation: (Int) -> Unit
) : OrientationEventListener(context) {
    private val display = (context.getSystemService(WINDOW_SERVICE) as WindowManager).defaultDisplay
    private var lastAngle = -1

    override fun enable() {
        super.enable()
        onOrientationChanged(0)
    }

    override fun disable() {
        super.disable()
        lastAngle = -1
    }

    override fun onOrientationChanged(orientation: Int) {
        if (display.angle != lastAngle) {
            lastAngle = display.angle
            val angle = when (config.facing) {
                // Accounts for mirrored front cameras
                CAMERA_FACING_FRONT -> (360 - (config.orientation + lastAngle) % 360) % 360
                else -> (config.orientation - lastAngle + 360) % 360
            }
            onRotation(angle)
        }
    }
}

private val Display.angle: Int
    get() = when (rotation) {
        ROTATION_90 -> 90
        ROTATION_180 -> 180
        ROTATION_270 -> 270
        else -> 0
    }
