package com.barcodedetectorview.internal

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

internal class TaskScheduler(val threadCount: Int = 1) {
    private val executor: ExecutorService = Executors.newFixedThreadPool(threadCount)
    private val cancellableTasks = mutableListOf<Future<*>>()

    fun execute(isCancellable: Boolean = false, task: () -> Unit) {
        val future = executor.submit(task)
        if (isCancellable) cancellableTasks += future
        cancellableTasks.removeAll { !it.isPending }
    }

    fun cancelPending() {
        cancellableTasks.filter { it.isPending }.forEach { it.cancel(true) }
        cancellableTasks.clear()
    }

    fun shutdown() {
        executor.shutdown()
        try {
            if (!executor.awaitTermination(5, TimeUnit.SECONDS)) executor.shutdownNow()
        } catch (e: InterruptedException) {
            executor.shutdownNow()
            Thread.currentThread().interrupt()
        }
    }
}

private val Future<*>.isPending get() = !isCancelled && !isDone
