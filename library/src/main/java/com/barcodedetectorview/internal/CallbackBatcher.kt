package com.barcodedetectorview.internal

import android.os.Handler
import android.util.SparseArray
import com.barcodedetectorview.Barcode
import com.barcodedetectorview.BarcodeDetectorView
import java.util.concurrent.ArrayBlockingQueue
import com.google.android.gms.vision.barcode.Barcode as GmsBarcode

internal class CallbackBatcher {
    val callbacks = mutableListOf<BarcodeDetectorView.Callback>()
    var threadCount = 0
        set(value) {
            field = value
            queue = ArrayBlockingQueue(field)
            for (i in 0 until field) queue.offer(mutableListOf())
        }

    private lateinit var queue: ArrayBlockingQueue<MutableList<Barcode>>

    private val handler = Handler()

    fun onSuccess(gmsBarcodes: SparseArray<GmsBarcode>) {
        val barcodes = queue.poll()
        barcodes.clear()

        for (i in 0 until gmsBarcodes.size()) {
            val value = gmsBarcodes.valueAt(i)
            Barcode.from(value)?.let(barcodes::add)
        }
        if (barcodes.isNotEmpty()) {
            handler.post {
                callbacks.forEach { it.onDetected(barcodes) }
                queue.offer(barcodes)
            }
        }
    }

    fun onError(t: Throwable) {
        handler.post {
            callbacks.forEach { it.onError(t) }
        }
    }
}
