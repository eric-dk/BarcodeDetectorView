@file:Suppress("deprecation")

package com.barcodedetectorview.internal

import android.content.Context
import android.graphics.ImageFormat
import android.hardware.Camera
import com.barcodedetectorview.Barcode
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.Frame.*
import com.google.android.gms.vision.barcode.BarcodeDetector
import java.nio.ByteBuffer
import java.util.*

internal class ImageProcessor(private val batcher: CallbackBatcher) : Camera.PreviewCallback {
    var angle = ROTATION_0
        set(value) {
            field = when (value) {
                90 -> ROTATION_90
                180 -> ROTATION_180
                270 -> ROTATION_270
                else -> ROTATION_0
            }
        }

    private lateinit var detector: BarcodeDetector
    private lateinit var config: Camera.Parameters
    private lateinit var scheduler: TaskScheduler

    private val buffers = mutableMapOf<ByteArray, ByteBuffer>()
    private var lastCapacity = -1
    private var lastFilter: EnumSet<Barcode.Format>? = null

    fun attachToCamera(context: Context,
                       camera: Camera,
                       filter: EnumSet<Barcode.Format>,
                       threadCount: Int) {
        if (!::scheduler.isInitialized || threadCount != scheduler.threadCount) {
            scheduler = TaskScheduler(threadCount)
        }
        if (filter != lastFilter) {
            detector = BarcodeDetector.Builder(context)
                    .setBarcodeFormats(filter.toInt())
                    .build()
            lastFilter = filter
        }

        allocateBuffers(camera, threadCount)
        camera.setPreviewCallbackWithBuffer(this)
    }

    private fun allocateBuffers(camera: Camera,
                                threadCount: Int) {
        config = camera.parameters
        val size = config.previewSize
        val capacity =
                size.width * size.height * ImageFormat.getBitsPerPixel(config.previewFormat) / 8

        capacity != lastCapacity || return
        buffers.clear()
        for (i in 0..threadCount) {
            val buffer = ByteBuffer.allocate(capacity)
            buffers[buffer.array()] = buffer
            camera.addCallbackBuffer(buffer.array())
        }
        lastCapacity = capacity
    }

    fun detachFromCamera() {
        detector.release()
        scheduler.cancelPending()
    }

    fun shutdown() = scheduler.shutdown()

    override fun onPreviewFrame(data: ByteArray,
                                camera: Camera) {
        scheduler.execute(true) {
            if (detector.isOperational) detectInFrame(data)
            camera.addCallbackBuffer(data)
        }
    }

    private fun detectInFrame(data: ByteArray) {
        buffers[data]?.let { buffer ->
            val size = config.previewSize
            val frame = Frame.Builder()
                    .setImageData(buffer, size.width, size.height, config.previewFormat)
                    .build()
            try {
                val results = detector.detect(frame)
                if (results.size() != 0) batcher.onSuccess(results)
            } catch (e: Exception) {
                batcher.onError(e)
            }
        }
    }
}
