@file:Suppress("deprecation")

package com.barcodedetectorview.internal

import android.content.Context
import android.hardware.Camera
import android.hardware.Camera.CameraInfo.CAMERA_FACING_BACK
import android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
import android.hardware.Camera.Parameters.*
import com.barcodedetectorview.Barcode
import com.barcodedetectorview.BarcodeDetectorView
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.math.max
import kotlin.math.min

internal class CameraDevice(context: Context,
                            private val batcher: CallbackBatcher
) {
    val view get() = preview.view

    private lateinit var camera: Camera

    private var isRunning = AtomicBoolean()
    private val preview = CameraPreview(context)
    private val processor = ImageProcessor(batcher)
    private val scheduler = TaskScheduler()

    private val config = Camera.CameraInfo()
    private val sensor = RotationSensor(context, config) { angle ->
        scheduler.execute(true) {
            camera.setDisplayOrientation(angle)
            processor.angle = angle
        }
    }

    fun start(context: Context,
              which: BarcodeDetectorView.Camera,
              filter: EnumSet<Barcode.Format>,
              threadCount: Int) {
        scheduler.execute {
            isRunning.compareAndSet(false, true) || return@execute
            try {
                camera = open(which)

                val config = camera.parameters
                config.antibanding = config.bestAntiBanding
                config.focusMode = config.bestFocusMode
                config.setBestPreviewSize(view.width, view.height)
                camera.parameters = config

                preview.attachToCamera(camera)
                processor.attachToCamera(context, camera, filter, threadCount)
                sensor.enable()

                camera.startPreview()
                if (config.focusMode == FOCUS_MODE_AUTO) camera.autoFocus(null)
            } catch (e: Exception) {
                batcher.onError(e)
            }
        }
    }

    private fun open(which: BarcodeDetectorView.Camera): Camera {
        val facing = when (which) {
            BarcodeDetectorView.Camera.FRONT -> CAMERA_FACING_FRONT
            BarcodeDetectorView.Camera.BACK -> CAMERA_FACING_BACK
        }
        for (id in 0 until Camera.getNumberOfCameras()) {
            Camera.getCameraInfo(id, config)
            if (config.facing == facing) return Camera.open(id)
        }
        throw RuntimeException("Cannot find $which camera")
    }

    fun stop() {
        scheduler.execute {
            isRunning.compareAndSet(true, false) || return@execute
            try {
                preview.detachFromCamera()
                processor.detachFromCamera()
                sensor.disable()

                camera.stopPreview()
                camera.release()
            } catch (e: Exception) {
                batcher.onError(e)
            }
        }
    }

    fun restart(context: Context,
                which: BarcodeDetectorView.Camera,
                filter: EnumSet<Barcode.Format>,
                threadCount: Int) {
        isRunning.get() || return
        scheduler.cancelPending()
        stop()
        start(context, which, filter, threadCount)
    }

    fun shutdown() {
        processor.shutdown()
        scheduler.shutdown()
    }
}

private val Camera.Parameters.bestAntiBanding: String
    get() {
        val supported = supportedAntibanding ?: return ANTIBANDING_OFF
        return when {
            ANTIBANDING_AUTO in supported -> ANTIBANDING_AUTO
            ANTIBANDING_50HZ in supported -> ANTIBANDING_50HZ
            ANTIBANDING_60HZ in supported -> ANTIBANDING_60HZ
            else -> ANTIBANDING_OFF
        }
    }

private val Camera.Parameters.bestFocusMode: String
    get() {
        val supported = supportedFocusModes ?: return FOCUS_MODE_INFINITY
        return when {
            FOCUS_MODE_CONTINUOUS_PICTURE in supported -> FOCUS_MODE_CONTINUOUS_PICTURE
            FOCUS_MODE_AUTO in supported -> FOCUS_MODE_AUTO
            FOCUS_MODE_FIXED in supported -> FOCUS_MODE_FIXED
            else -> FOCUS_MODE_INFINITY
        }
    }

private fun Camera.Parameters.setBestPreviewSize(width: Int,
                                                 height: Int) {
    val x = max(width, height)
    val y = min(width, height)
    val sizes = supportedPreviewSizes

    sizes.sortBy { it.width * it.height }
    val size = sizes.find { it.width >= x && it.height >= y } ?: sizes.last()
    setPreviewSize(size.width, size.height)
}
