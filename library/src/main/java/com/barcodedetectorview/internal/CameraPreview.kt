@file:Suppress("deprecation")

package com.barcodedetectorview.internal

import android.content.Context
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.N
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.TextureView
import java.util.concurrent.CountDownLatch

internal class CameraPreview(context: Context
) : SurfaceHolder.Callback, TextureView.SurfaceTextureListener {
    val view = when {
        SDK_INT >= N -> SurfaceView(context).also { it.holder.addCallback(this) }
        else -> TextureView(context).also { it.surfaceTextureListener = this }
    }

    private var camera: Camera? = null
    private val latch = CountDownLatch(1)

    fun attachToCamera(camera: Camera) {
        latch.await()
        when (view) {
            is SurfaceView -> camera.setPreviewDisplay(view.holder)
            is TextureView -> camera.setPreviewTexture(view.surfaceTexture)
        }
        this.camera = camera
    }

    fun detachFromCamera() {
        try {
            when (view) {
                is SurfaceView -> camera?.setPreviewDisplay(null)
                is TextureView -> camera?.setPreviewTexture(null)
            }
        } finally {
            camera = null
        }
    }

    override fun surfaceCreated(holder: SurfaceHolder?) = latch.countDown()

    override fun surfaceChanged(holder: SurfaceHolder?,
                                format: Int,
                                width: Int,
                                height: Int) {}

    override fun surfaceDestroyed(holder: SurfaceHolder?) = detachFromCamera()

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?,
                                           width: Int,
                                           height: Int) {
        latch.countDown()
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?,
                                             width: Int,
                                             height: Int) {}

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
        detachFromCamera()
        return true
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {}
}
