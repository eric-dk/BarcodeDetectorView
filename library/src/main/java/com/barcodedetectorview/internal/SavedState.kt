package com.barcodedetectorview.internal

import android.os.Parcel
import android.os.Parcelable
import android.view.AbsSavedState
import android.view.View
import com.barcodedetectorview.Barcode
import com.barcodedetectorview.BarcodeDetectorView
import java.util.*

internal class SavedState : View.BaseSavedState {
    val camera: BarcodeDetectorView.Camera
    val formatFilter: EnumSet<Barcode.Format>
    val threadCount: Int

    constructor(superState: Parcelable?,
                facing: BarcodeDetectorView.Camera,
                formatFilter: EnumSet<Barcode.Format>,
                threadCount: Int
    ) : super(superState ?: AbsSavedState.EMPTY_STATE) {
        this.camera = facing
        this.formatFilter = formatFilter
        this.threadCount = threadCount
    }

    private constructor(source: Parcel) : super(source) {
        camera = source.readEnum()
        formatFilter = source.readInt().toFilter()
        threadCount = source.readInt()
    }

    override fun writeToParcel(out: Parcel, flags: Int) {
        super.writeToParcel(out, flags)
        out.writeEnum(camera)
        out.writeInt(formatFilter.toInt())
        out.writeInt(threadCount)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::SavedState)
    }
}
