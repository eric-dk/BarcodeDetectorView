package com.barcodedetectorview.internal

import android.content.res.TypedArray
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.StyleableRes
import com.barcodedetectorview.Barcode
import java.util.*
import com.google.android.gms.vision.barcode.Barcode as GmsBarcode

internal fun GmsBarcode.CalendarDateTime.toDate(): Date {
    val zone = if (isUtc) TimeZone.getTimeZone("UTC") else TimeZone.getDefault()
    val calendar = Calendar.getInstance(zone)

    calendar.set(year, month, day)
    if (hours > -1) calendar.set(Calendar.HOUR_OF_DAY, hours)
    if (minutes > -1) calendar.set(Calendar.MINUTE, minutes)
    if (seconds > -1) calendar.set(Calendar.SECOND, seconds)

    return calendar.time
}

internal fun EnumSet<Barcode.Format>.toInt(): Int {
    var result = -1
    if (Barcode.Format.AZTEC in this)           result = result or GmsBarcode.AZTEC
    if (Barcode.Format.CODABAR in this)         result = result or GmsBarcode.CODABAR
    if (Barcode.Format.CODE_128 in this)        result = result or GmsBarcode.CODE_128
    if (Barcode.Format.CODE_39 in this)         result = result or GmsBarcode.CODE_39
    if (Barcode.Format.CODE_93 in this)         result = result or GmsBarcode.CODE_93
    if (Barcode.Format.DATA_MATRIX in this)     result = result or GmsBarcode.DATA_MATRIX
    if (Barcode.Format.EAN_13 in this)          result = result or GmsBarcode.EAN_13
    if (Barcode.Format.EAN_8 in this)           result = result or GmsBarcode.EAN_8
    if (Barcode.Format.ITF in this)             result = result or GmsBarcode.ITF
    if (Barcode.Format.PDF417 in this)          result = result or GmsBarcode.PDF417
    if (Barcode.Format.QR_CODE in this)         result = result or GmsBarcode.QR_CODE
    if (Barcode.Format.UPC_A in this)           result = result or GmsBarcode.UPC_A
    if (Barcode.Format.UPC_E in this)           result = result or GmsBarcode.UPC_E
    return result
}

internal fun Int.toFilter(): EnumSet<Barcode.Format> {
    this != GmsBarcode.ALL_FORMATS || return EnumSet.allOf(Barcode.Format::class.java)

    val filter = EnumSet.noneOf(Barcode.Format::class.java)
    if (this has GmsBarcode.AZTEC)          filter.add(Barcode.Format.AZTEC)
    if (this has GmsBarcode.CODABAR)        filter.add(Barcode.Format.CODABAR)
    if (this has GmsBarcode.CODE_128)       filter.add(Barcode.Format.CODE_128)
    if (this has GmsBarcode.CODE_39)        filter.add(Barcode.Format.CODE_39)
    if (this has GmsBarcode.CODE_93)        filter.add(Barcode.Format.CODE_93)
    if (this has GmsBarcode.DATA_MATRIX)    filter.add(Barcode.Format.DATA_MATRIX)
    if (this has GmsBarcode.EAN_13)         filter.add(Barcode.Format.EAN_13)
    if (this has GmsBarcode.EAN_8)          filter.add(Barcode.Format.EAN_8)
    if (this has GmsBarcode.ITF)            filter.add(Barcode.Format.ITF)
    if (this has GmsBarcode.PDF417)         filter.add(Barcode.Format.PDF417)
    if (this has GmsBarcode.QR_CODE)        filter.add(Barcode.Format.QR_CODE)
    if (this has GmsBarcode.UPC_A)          filter.add(Barcode.Format.UPC_A)
    if (this has GmsBarcode.UPC_E)          filter.add(Barcode.Format.UPC_E)
    return filter
}

private infix fun Int.has(mask: Int) = this or mask == this

internal inline fun <reified T : Parcelable> parcelableCreator(crossinline create: (Parcel) -> T
): Parcelable.Creator<T> {
    return object : Parcelable.Creator<T> {
        override fun createFromParcel(source: Parcel) = create(source)
        override fun newArray(size: Int) = arrayOfNulls<T>(size)
    }
}

internal fun Parcel.readBoolean(): Boolean = readInt() == 1

internal fun Parcel.writeBoolean(value: Boolean) = writeInt(if (value) 1 else 0)

internal inline fun <reified T : Enum<T>> Parcel.readEnum(): T = enumValues<T>()[readInt()]

internal fun <T : Enum<T>> Parcel.writeEnum(value: T) = writeInt(value.ordinal)

internal inline fun <reified T : Enum<T>> TypedArray.getEnum(@StyleableRes index: Int,
                                                             defValue: T
): T {
    val value = getInt(index, -1)
    return if (value != -1) enumValues<T>()[value] else defValue
}

internal fun TypedArray.getFilter(@StyleableRes index: Int,
                                  defValue: EnumSet<Barcode.Format>
): EnumSet<Barcode.Format> {
    val value = getInt(index, -1)
    return if (value != -1) value.toFilter() else defValue
}
