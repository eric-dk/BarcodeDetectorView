package com.barcodedetectorview

import android.content.Context
import android.graphics.Color
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import com.barcodedetectorview.internal.*
import java.util.*
import kotlin.math.max

class BarcodeDetectorView
@JvmOverloads constructor(context: Context,
                          attrs: AttributeSet? = null,
                          @AttrRes defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    interface Callback {
        /**
         *
         */
        fun onDetected(barcodes: List<Barcode>)

        /**
         *
         */
        fun onError(t: Throwable) {
            val message = t.message ?: t::class.java.simpleName
            Log.e(BarcodeDetectorView::class.java.simpleName, message, t)
        }
    }

    /**
     *
     */
    fun start() = device.start(context, camera, formatFilter, threadCount)

    /**
     *
     */
    fun stop() = device.stop()

    /**
     *
     */
    fun shutdown() = device.shutdown()

    /**
     *
     */
    fun addCallback(callback: Callback) {
        messenger.callbacks += callback
    }

    /**
     *
     */
    fun removeCallback(callback: Callback) {
        messenger.callbacks -= callback
    }

    enum class Camera { BACK, FRONT }

    var camera = Camera.BACK
        set(value) {
            field = value
            device.restart(context, field, formatFilter, threadCount)
        }
    var formatFilter: EnumSet<Barcode.Format> = EnumSet.allOf(Barcode.Format::class.java)
        set(value) {
            field = EnumSet.copyOf(value)
            device.restart(context, camera, field, threadCount)
        }
    var threadCount = max(Runtime.getRuntime().availableProcessors() / 2, 1)
        set(value) {
            field = value
            messenger.threadCount = field
            device.restart(context, camera, formatFilter, field)
        }

    private val messenger = CallbackBatcher()
    private val device = CameraDevice(context, messenger)

    init {
        val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.BarcodeDetectorView,
                defStyleAttr,
                0
        )
        try {
            val background = typedArray.getResourceId(
                    R.styleable.BarcodeDetectorView_android_background,
                    -1
            )
            if (background != -1) setBackgroundResource(background)
            else setBackgroundColor(Color.BLACK)

            camera = typedArray.getEnum(R.styleable.BarcodeDetectorView_camera, camera)
            formatFilter = typedArray
                    .getFilter(R.styleable.BarcodeDetectorView_formatFilter, formatFilter)
            threadCount = typedArray
                    .getInt(R.styleable.BarcodeDetectorView_threadCount, threadCount)
                    .coerceIn(1..Runtime.getRuntime().availableProcessors())
        } finally {
            typedArray.recycle()
        }
        if (!isInEditMode) addView(device.view)
    }

    override fun onSaveInstanceState(): Parcelable {
        return SavedState(super.onSaveInstanceState(), camera, formatFilter, threadCount)
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            camera = state.camera
            formatFilter = state.formatFilter
            threadCount = state.threadCount
        } else {
            super.onRestoreInstanceState(state)
        }
    }
}
