package com.barcodedetectorview

import android.os.Parcel
import android.os.Parcelable
import com.barcodedetectorview.internal.*
import java.util.*
import com.google.android.gms.vision.barcode.Barcode as GmsBarcode

sealed class Barcode : Parcelable {
    enum class Format {
        AZTEC,
        CODABAR,
        CODE_39,
        CODE_93,
        CODE_128,
        DATA_MATRIX,
        EAN_8,
        EAN_13,
        ITF,
        PDF417,
        QR_CODE,
        UPC_A,
        UPC_E
    }

    val format: Format
    val value: String

    constructor(format: Format,
                value: String) {
        this.format = format
        this.value = value
    }

    constructor(parcel: Parcel) {
        format = parcel.readEnum()
        value = parcel.readString()!!
    }

    override fun equals(other: Any?) = when {
        this === other -> true
        other !is Barcode -> false
        else -> value == other.value
    }

    override fun hashCode() = value.hashCode()

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        dest.writeEnum(format)
        dest.writeString(value)
    }

    internal companion object {
        fun from(barcode: GmsBarcode): Barcode? {
            val filter = barcode.format.toFilter()
            filter.size == 1 || return null

            val format = filter.first()
            val value = barcode.rawValue ?: return null

            return when (barcode.valueFormat) {
                GmsBarcode.CALENDAR_EVENT ->
                    Event(format, value, barcode.calendarEvent ?: return null)
                GmsBarcode.CONTACT_INFO ->
                    Contact(format, value, barcode.contactInfo ?: return null)
                GmsBarcode.EMAIL ->
                    Email(format, value, barcode.email ?: return null)
                GmsBarcode.DRIVER_LICENSE ->
                    IdCard(format, value, barcode.driverLicense ?: return null)
                GmsBarcode.GEO ->
                    Location(format, value, barcode.geoPoint ?: return null)
                GmsBarcode.PHONE ->
                    Phone(format, value, barcode.phone ?: return null)
                GmsBarcode.SMS ->
                    Sms(format, value, barcode.sms ?: return null)
                GmsBarcode.URL ->
                    Bookmark(format, value, barcode.url ?: return null)
                GmsBarcode.WIFI ->
                    WiFi(format, value, barcode.wifi ?: return null)
                else ->
                    Text(format, value, barcode.displayValue ?: "")
            }
        }
    }
}

/**
 *
 */
class Bookmark : Barcode {
    val title: String?
    val url: String?

    internal constructor(format: Format,
                         value: String,
                         bookmark: GmsBarcode.UrlBookmark
    ) : super(format, value) {
        title = bookmark.title
        url = bookmark.url
    }

    private constructor(parcel: Parcel) : super(parcel) {
        title = parcel.readString()
        url = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(title)
        dest.writeString(url)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Bookmark)
    }
}

/**
 *
 */
class Contact : Barcode {
    class Address : Parcelable {
        /*
         * UNKNOWN      0
         * WORK         1
         * HOME         2
         */
        enum class Type { NOT_SPECIFIED, WORK, HOME }

        val lines: List<String>
        val type: Type

        internal constructor(address: GmsBarcode.Address) {
            lines = address.addressLines.toList()
            type = enumValues<Type>()[address.type]
        }

        private constructor(parcel: Parcel) {
            lines = parcel.createStringArrayList()!!
            type = parcel.readEnum()
        }

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel,
                                   flags: Int) {
            dest.writeStringList(lines)
            dest.writeEnum(type)
        }

        companion object {
            @JvmField val CREATOR = parcelableCreator(::Address)
        }
    }

    class Name : Parcelable {
        val formatted: String?
        val pronunciation: String?
        val prefix: String?
        val first: String?
        val middle: String?
        val last: String?
        val suffix: String?

        internal constructor(name: GmsBarcode.PersonName) {
            formatted = name.formattedName
            pronunciation = name.pronunciation
            prefix = name.prefix
            first = name.first
            middle = name.middle
            last = name.last
            suffix = name.suffix
        }

        private constructor(parcel: Parcel) {
            formatted = parcel.readString()
            pronunciation = parcel.readString()
            prefix = parcel.readString()
            first = parcel.readString()
            middle = parcel.readString()
            last = parcel.readString()
            suffix = parcel.readString()
        }

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel,
                                   flags: Int) {
            dest.writeString(formatted)
            dest.writeString(pronunciation)
            dest.writeString(prefix)
            dest.writeString(first)
            dest.writeString(middle)
            dest.writeString(last)
            dest.writeString(suffix)
        }

        companion object {
            @JvmField val CREATOR = parcelableCreator(::Name)
        }
    }

    val name: Name?
    val organization: String?
    val title: String?
    val phones: List<Phone>
    val emails: List<Email>
    val addresses: List<Address>
    val urls: List<String>

    internal constructor(format: Format,
                         value: String,
                         info: GmsBarcode.ContactInfo
    ) : super(format, value) {
        name = Name(info.name)
        organization = info.organization
        title = info.title
        phones = info.phones.map { Phone(format, value, it) }
        emails = info.emails.map { Email(format, value, it) }
        addresses = info.addresses.map { Address(it) }
        urls = info.urls.toList()
    }

    private constructor(parcel: Parcel) : super(parcel) {
        name = parcel.readParcelable(Name::class.java.classLoader)
        organization = parcel.readString()
        title = parcel.readString()
        phones = listOf<Phone>().also { parcel.readList(it, Phone::class.java.classLoader) }
        emails = listOf<Email>().also { parcel.readList(it, Email::class.java.classLoader) }
        addresses = listOf<Address>().also { parcel.readList(it, Address::class.java.classLoader) }
        urls = parcel.createStringArrayList()!!
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeParcelable(name, 0)
        dest.writeString(organization)
        dest.writeString(title)
        dest.writeList(phones)
        dest.writeList(emails)
        dest.writeList(addresses)
        dest.writeStringList(urls)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Contact)
    }
}

/**
 *
 */
class Email : Barcode {
    /*
     * UNKNOWN      0
     * WORK         1
     * HOME         2
     */
    enum class Type { NOT_SPECIFIED, WORK, HOME }

    val address: String?
    val body: String?
    val subject: String?
    val type: Type

    internal constructor(format: Format,
                         value: String,
                         email: GmsBarcode.Email
    ) : super (format, value) {
        address = email.address
        body = email.body
        subject = email.subject
        type = enumValues<Type>()[email.type]
    }

    private constructor(parcel: Parcel) : super(parcel) {
        address = parcel.readString()
        body = parcel.readString()
        subject = parcel.readString()
        type = parcel.readEnum()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(address)
        dest.writeString(body)
        dest.writeString(subject)
        dest.writeEnum(type)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Email)
    }
}

/**
 *
 */
class Event : Barcode {
    val begin: Date?
    val end: Date?
    val description: String?
    val location: String?
    val organizer: String?
    val status: String?
    val summary: String?

    internal constructor(format: Format,
                         value: String,
                         event: GmsBarcode.CalendarEvent
    ) : super(format, value) {
        begin = event.start?.toDate()
        end = event.end?.toDate()
        description = event.description
        location = event.location
        organizer = event.organizer
        status = event.status
        summary = event.summary
    }

    private constructor(parcel: Parcel) : super(parcel) {
        begin = parcel.readSerializable() as Date
        end = parcel.readSerializable() as Date
        description = parcel.readString()
        location = parcel.readString()
        organizer = parcel.readString()
        status = parcel.readString()
        summary = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeSerializable(begin)
        dest.writeSerializable(end)
        dest.writeString(description)
        dest.writeString(location)
        dest.writeString(organizer)
        dest.writeString(status)
        dest.writeString(summary)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Event)
    }
}

/**
 *
 */
class IdCard : Barcode {
    val isDriverLicense: Boolean
    val firstName: String?
    val middleName: String?
    val lastName: String?
    val gender: String?
    val street: String?
    val city: String?
    val state: String?
    val country: String?
    val zip: String?
    val number: String?
    val issueDate: String?
    val expiryDate: String?
    val birthDate: String?

    internal constructor(format: Format,
                         value: String,
                         license: GmsBarcode.DriverLicense
    ) : super(format, value) {
        isDriverLicense = license.documentType == "DL"
        firstName = license.firstName
        middleName = license.middleName
        lastName = license.lastName
        gender = license.gender
        street = license.addressStreet
        city = license.addressCity
        state = license.addressState
        country = license.issuingCountry
        zip = license.addressZip
        number = license.licenseNumber
        issueDate = license.issueDate
        expiryDate = license.expiryDate
        birthDate = license.birthDate
    }

    private constructor(parcel: Parcel) : super(parcel) {
        isDriverLicense = parcel.readBoolean()
        firstName = parcel.readString()
        middleName = parcel.readString()
        lastName = parcel.readString()
        gender = parcel.readString()
        street = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        country = parcel.readString()
        zip = parcel.readString()
        number = parcel.readString()
        issueDate = parcel.readString()
        expiryDate = parcel.readString()
        birthDate = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeBoolean(isDriverLicense)
        dest.writeString(firstName)
        dest.writeString(middleName)
        dest.writeString(lastName)
        dest.writeString(gender)
        dest.writeString(street)
        dest.writeString(city)
        dest.writeString(state)
        dest.writeString(country)
        dest.writeString(zip)
        dest.writeString(number)
        dest.writeString(issueDate)
        dest.writeString(expiryDate)
        dest.writeString(birthDate)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::IdCard)
    }
}

/**
 *
 */
class Location : Barcode {
    val latitude: Double
    val longitude: Double

    internal constructor(format: Format,
                         value: String,
                         geo: GmsBarcode.GeoPoint
    ) : super(format, value) {
        latitude = geo.lat
        longitude = geo.lng
    }

    private constructor(parcel: Parcel) : super(parcel) {
        latitude = parcel.readDouble()
        longitude = parcel.readDouble()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeDouble(latitude)
        dest.writeDouble(longitude)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Location)
    }
}

/**
 *
 */
class Phone : Barcode {
    /*
     * UNKNOWN      0
     * WORK         1
     * HOME         2
     * FAX          3
     * MOBILE       4
     */
    enum class Type { NOT_SPECIFIED, WORK, HOME, FAX, MOBILE }

    val number: String?
    val type: Type

    internal constructor(format: Format,
                         value: String,
                         phone: GmsBarcode.Phone
    ) : super(format, value) {
        number = phone.number
        type = enumValues<Type>()[phone.type]
    }

    private constructor(parcel: Parcel) : super(parcel) {
        number = parcel.readString()
        type = parcel.readEnum()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(number)
        dest.writeEnum(type)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Phone)
    }
}

/**
 *
 */
class Sms : Barcode {
    val message: String?
    val phoneNumber: String?

    internal constructor(format: Format,
                         value: String,
                         sms: GmsBarcode.Sms
    ) : super(format, value) {
        message = sms.message
        phoneNumber = sms.phoneNumber
    }

    private constructor(parcel: Parcel) : super(parcel) {
        message = parcel.readString()
        phoneNumber = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(message)
        dest.writeString(phoneNumber)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Sms)
    }
}

/**
 *
 */
class Text : Barcode {
    val text: String

    internal constructor(format: Format,
                         value: String,
                         text: String
    ) : super(format, value) {
        this.text = text
    }

    private constructor(parcel: Parcel) : super(parcel) {
        text = parcel.readString()!!
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(text)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::Text)
    }
}

/**
 *
 */
class WiFi : Barcode {
    /*
     * OPEN         1
     * WPA          2
     * WEP          3
     */
    enum class Security { OPEN, WPA, WEP }

    val password: String?
    val security: Security
    val ssid: String?

    internal constructor(format: Format,
                         value: String,
                         wifi: GmsBarcode.WiFi
    ) : super(format, value) {
        password = wifi.password
        security = enumValues<Security>()[wifi.encryptionType - 1]
        ssid = wifi.ssid
    }

    private constructor(parcel: Parcel) : super(parcel) {
        password = parcel.readString()
        security = parcel.readEnum()
        ssid = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel,
                               flags: Int) {
        super.writeToParcel(dest, flags)
        dest.writeString(password)
        dest.writeEnum(security)
        dest.writeString(ssid)
    }

    companion object {
        @JvmField val CREATOR = parcelableCreator(::WiFi)
    }
}
